<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes" />

  <xsl:template match="/Request">
    <BankRequest>
      <xsl:apply-templates select="Header" />
      <xsl:apply-templates select="PersonList" />
    </BankRequest>
  </xsl:template>

  <xsl:template match="Header">
    <Head>
      <xsl:apply-templates select="MessageID" />
      <xsl:apply-templates select="MessageDateTime" />
      <xsl:apply-templates select="MessageDateTime" />
      <SendFromTo>
        <xsl:value-of select="Sender/ID"></xsl:value-of>
        -
        <xsl:value-of select="Target/ID"></xsl:value-of>
      </SendFromTo>
    </Head>
  </xsl:template>

  <xsl:template match="MessageID">
    <MsgID>
      <xsl:value-of select="MessageID" />
    </MsgID>
  </xsl:template>

  <xsl:template match="MessageDateTime">
    <MsgDate>
      <xsl:variable name="msgDT" select="MessageDateTime" />
      <xsl:value select="format-dateTime($msgDT, '[Y0001]/[M01]/[D01]  [H01]:[m01]:[s01]')" />
    </MsgDate>
  </xsl:template>

  <xsl:template match="PersonList">
    <Persons>
      <xsl:for-each select="Person">
        <FIO>
          <xsl:value-of select="FirstName" />
          <xsl:value-of select="MiddleName" />
          <xsl:if test="LastName">
            <xsl:value-of select="LastName" />
          </xsl:if>
        </FIO>
        <DateOfBirth>
          <xsl:variable name="bDT" select="BirthDate" />
          <xsl:value select="format-dateTime($bDT, '[D01]/[M01]/[Y0001]" />
        </DateOfBirth>
        <xsl:apply-templates select="EducationList" />
        <xsl:apply-templates select="OperatorInfo" />
      </xsl:for-each>
    </Persons>
  </xsl:template>

  <xsl:template match="EducationList">
    <Educations>
      <xsl:for-each select="Education">
        <EducationName>
          <xsl:value-of select="Name" />
        </EducationName>
        <EndDate>
          <xsl:variable name="eDT" select="Date" />
          <xsl:value select="format-dateTime($eDT, '[D01]/[M01]/[Y0001]" />
        </EndDate>
      </xsl:for-each>
    </Educations>
  </xsl:template>

  <xsl:template match="OperatorInfo">
    <Information>
      <UserName>
        <xsl:variable name="length" select="string-length(Login)" />
        <xsl:choice>
          <xsl:when test="$length > 9">
            <xsl:variable name="first" select="substring(Login, 0, 5)" />
            <xsl:variable name="last" select="substring(Login, $length - 4, 3)" />
            <xsl:value-of select="$first" />
            _
            <xsl:value-of select="$last" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="Login" />
          </xsl:otherwise>
        </xsl:choice>
      </UserName>
      <InfoDate>
        <xsl:variable name="dT" select="DateTime" />
        <xsl:value select="format-dateTime($dT, '[Y0001]/[M01]/[D01]  [H01]:[m01]:[s01]')" />
      </InfoDate>
      <InfoComment>
        <xsl:choice>
          <xsl:when test="Comment = 'False'">0</xsl:when>
          <xsl:when test="Comment = 'Limited'">1</xsl:when>
          <xsl:otherwise>2</xsl:otherwise>
        </xsl:choice>
      </InfoComment>
    </Information>
  </xsl:template>
</xsl:stylesheet>